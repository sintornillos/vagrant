#!/usr/bin/env bash
DIRECTORIO=/var/www/localhost/htdocs/talleres
DIRHTML=/var/www/localhost/htdocs/

apk update

apk add apache2
apk add git

service apache2 start

if [ -d "$DIRECTORIO"  ] ; then
	echo "El DIRHTML ${DIRHTML} existe "
	cd $DIRHTML
	pwd
	git pull 
else
	echo "No existe el DIRHTML ${DIRECTORIO} "
	rm -rf $DIRHTML
	echo "Clono el repositorio"
	git clone http://github.com/chrodriguez/arquitecturas-de-software.git /var/www/localhost/htdocs/
	echo "..."
fi
